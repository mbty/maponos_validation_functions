# ![logo](https://i.imgur.com/ryJj2DQ.png) Maponos validation functions
A repository of ready-to-use validation functions for
[Maponos](https://gitlab.com/mbty/maponos)

## Contents
- `validate_unsigned_int.cc`: strict validation of a single unsigned int
  argument

## Legal
The code in this repository is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it and/or modify
it under the terms of the Do What The Fuck You Want To Public License, Version
2, included in the LICENSE.md file.

## Future
More validation functions.
