// Strict validation of a single unsigned int argument

#include <algorithm>
#include <cctype>
#include <vector>

bool validate_unsigned_int(
  int argc, char *argv[], const maponos::contextualized_string &call_name,
  const std::vector<maponos::contextualized_string> &args, unsigned int &result
) {
  if (args.size() == 0) {
    maponos::print_error_location(
      argc, argv, call_name, "This option takes an argument"
    );
    return false;
  }

  if (args.size() != 1) {
    maponos::print_error_location(
      argc, argv, args[1], args[args.size() - 1],
      "Too many arguments (expected one)"
    );
    return false;
  }

  if (!std::all_of(args[0].begin(), args[0].end(), ::isdigit)) {
    maponos::print_error_location(
      argc, argv, args[0],
      "Could not convert to unsigned integer (wrong format, should be digits "
      "only)"
    );
    return false;
  }

  if (args[0][0] == '0') {
    maponos::print_error_location(
      argc, argv, args[0],
      "Could not convert to unsigned integer (wrong format, can't have "
      "leading zeros)"
    );
    return false;
  }

  unsigned long long_result;
  try {
    long_result = std::stoul(args[0]);
  }
  // Should never happen, see std::all_of above
  catch (std::invalid_argument &e) {
    maponos::print_error_location(
      argc, argv, args[0],
      "Could not convert to unsigned integer (wrong format)"
    );
    return false;
  }
  catch (std::out_of_range &e) {
    maponos::print_error_location(
      argc, argv, args[0],
      "Could not convert to unsigned integer (range error)"
    );
    return false;
  }

  // Catch cases that would result in a conversion error
  if (long_result > std::numeric_limits<unsigned int>::max()) {
    maponos::print_error_location(
      argc, argv, args[0],
      "Could not convert to unsigned integer (range error)"
    );
    return false;
  }

  // Safe: the overflow case is managed above
  result = static_cast<unsigned int>(long_result);
  return true;
}
